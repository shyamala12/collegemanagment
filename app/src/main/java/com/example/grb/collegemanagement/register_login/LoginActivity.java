package com.example.grb.collegemanagement.register_login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grb.collegemanagement.R;

public class LoginActivity extends AppCompatActivity {
    EditText userName, password;
    CheckBox cbStudent, cbTeacher, cbAdmin;
    Button btnLogin;
    TextView tvForgotPW, tvRegister;

    String userType="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initialiazation
        userName = findViewById(R.id.et_userid);
        password = findViewById(R.id.et_password);
        cbStudent = findViewById(R.id.cb_student);
        cbTeacher = findViewById(R.id.cb_teacher);
        cbAdmin = findViewById(R.id.cb_admin);
        btnLogin = findViewById(R.id.btn_login);
        tvRegister = findViewById(R.id.tv_register);

        cbStudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    userType = "student";
                    cbAdmin.setChecked(false);
                    cbTeacher.setChecked(false);
                }
                else {
                    userType = "";
                }
            }
        });

        cbTeacher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userType = "teacher";
                    cbAdmin.setChecked(false);
                    cbStudent.setChecked(false);
                }
                else {
                    userType = "";
                }
            }
        });

        cbAdmin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userType = "admin";
                    cbStudent.setChecked(false);
                    cbTeacher.setChecked(false);
                }
                else {
                    userType = "";
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str_username = String.valueOf(userName.getText());
                String str_password = String.valueOf(password.getText());

                doLogin(str_password, str_username, userType);
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doLogin(String pw, String un, String ut) {
        if (validate(pw, un, ut)) {
            Toast.makeText(LoginActivity.this, "Login detail verified", Toast.LENGTH_SHORT).show();
            // login logic

        }


    }

    private boolean validate(String pw, String un, String ut) {
        if (TextUtils.isEmpty(un)) {
            Toast.makeText(LoginActivity.this, "Please provide a valid Username", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(pw)) {
            Toast.makeText(LoginActivity.this, "Please provide a valid Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(ut)) {
            Toast.makeText(LoginActivity.this, "Please select a user type checkbox", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
