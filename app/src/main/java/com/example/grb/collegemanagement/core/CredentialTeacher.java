package com.example.grb.collegemanagement.core;

public class CredentialTeacher {
    private String userId;
    private String password;
    private long lastLoggedin;

    public static final String TABLE_NAME = "CredentialTeacher";
    public static final String COLOMN_ID = "user_id";
    public static final String COLOMN_PW = "user_pw";
    public static final String COLOMN_TS = "user_time";

    public static final String CREATE_TABLE= "CREATE TABLE "+TABLE_NAME+"("+
            COLOMN_ID + "TEXT PRIMARY KEY,"+
            COLOMN_PW + "TEXT,"+
            COLOMN_TS + "DATETIME DEFAULT CURRENT_TIMESTAMP)";

    public String getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public long getLastLoggedin() {
        return lastLoggedin;
    }
}
