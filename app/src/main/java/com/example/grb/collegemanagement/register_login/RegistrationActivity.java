package com.example.grb.collegemanagement.register_login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.grb.collegemanagement.R;
import com.example.grb.collegemanagement.core.CredentialAdmin;
import com.example.grb.collegemanagement.database.CollegemanagDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity {
    EditText userName, password, passwordR;
    CheckBox cbStudent, cbTeacher, cbAdmin;
    Button btnRegister;

    String userType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        //intialization
        userName = findViewById(R.id.et_username);
        password = findViewById(R.id.et_password);
        passwordR = findViewById(R.id.et_password_r);
        cbStudent = findViewById(R.id.cb_student);
        cbTeacher = findViewById(R.id.cb_teacher);
        cbAdmin = findViewById(R.id.cb_admin);
        btnRegister = findViewById(R.id.btn_register);


        cbStudent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userType = "student";
                    cbTeacher.setChecked(false);
                    cbAdmin.setChecked(false);
                } else {
                    userType = "";
                }

            }
        });

        cbTeacher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    userType = "teacher";
                    cbStudent.setChecked(false);
                    cbAdmin.setChecked(false);
                } else {
                    userType = "";
                }
            }
        });

        cbAdmin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userType = "admin";
                    cbStudent.setChecked(false);
                    cbTeacher.setChecked(false);
                } else {
                    userType = "";
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameText = String.valueOf(userName.getText());
                String createPassword = String.valueOf(password.getText());
                String confirmPassword = String.valueOf(passwordR.getText());

                if (validate(usernameText, createPassword, confirmPassword, userType)) {
                    Toast.makeText(RegistrationActivity.this, "Validation Successful", Toast.LENGTH_SHORT).show();
                    //registration logic
                    CollegemanagDatabase databaseHelper = new CollegemanagDatabase(RegistrationActivity.this);
                    switch (userType) {
                        case "student":
                            //Student registration

                            break;
                        case "teacher":
                            //Teacher registration

                            break;
                        case "admin":
                            //Admin registration
                            CredentialAdmin admin = new CredentialAdmin(usernameText,createPassword);
                            long l = databaseHelper.insertCredential(admin);
                            databaseHelper.close();

                            break;
                    }






                }

            }
        });


    }

    private boolean validate(String uName, String crtPW, String cnfPW, String userType) {

        if (TextUtils.isEmpty(uName)) {
            Toast.makeText(RegistrationActivity.this, "Please enter valid username", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!validatePassword(cnfPW))
            return false;
        if (!cnfPW.equals(crtPW)) {
            Toast.makeText(RegistrationActivity.this, "Confirm password should be same as password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (TextUtils.isEmpty(userType)) {
            Toast.makeText(RegistrationActivity.this, "Please select a user type", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validatePassword(String password) {
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(RegistrationActivity.this, "Please enter a password", Toast.LENGTH_SHORT).show();
            return false;
        }

        Pattern p = Pattern.compile(".*[A-Z].*");
        Matcher m = p.matcher(password);
        if (!m.matches()) {
            Toast.makeText(RegistrationActivity.this, "password should must contain at least one capital character", Toast.LENGTH_SHORT).show();
            return false;
        }

        Pattern p1 = Pattern.compile(".*[0-9].*");
        Matcher m1 = p1.matcher(password);
        if (!m1.matches()) {
            Toast.makeText(RegistrationActivity.this, "password should must contain at least one number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}

