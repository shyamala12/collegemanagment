package com.example.grb.collegemanagement.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.grb.collegemanagement.core.CredentialAdmin;
import com.example.grb.collegemanagement.core.CredentialStudent;
import com.example.grb.collegemanagement.core.CredentialTeacher;

public class CollegemanagDatabase extends SQLiteOpenHelper {
    final static private String DATABASE_NAME = "CollegeDatabase";
    final static private int DATABASE_VERSION = 1;


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CredentialStudent.CREATE_TABLE);
        db.execSQL(CredentialTeacher.CREATE_TABLE);
        db.execSQL(CredentialAdmin.CREATE_TABLE);
    }

    public CollegemanagDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public long insertCredential(CredentialStudent credentialStudent) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CredentialStudent.COLOMN_ID, credentialStudent.getUserId());
        values.put(CredentialStudent.COLOMN_PW, credentialStudent.getPassword());
        long id = database.insert(CredentialStudent.TABLE_NAME, null, values);
        database.close();
        return id;
    }

    public long insertCredential(CredentialTeacher credentialTeacher) {
        SQLiteDatabase database=this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CredentialTeacher.COLOMN_ID, credentialTeacher.getUserId());
        values.put(CredentialTeacher.COLOMN_PW, credentialTeacher.getPassword());
        long id = database.insert(CredentialTeacher.TABLE_NAME, null, values);
        database.close();
        return id;

    }

    public long insertCredential(CredentialAdmin credentialAdmin) {
        SQLiteDatabase database=this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CredentialAdmin.COLOMN_ID, credentialAdmin.getUserId());
        values.put(CredentialAdmin.COLOMN_PW, credentialAdmin.getPassword());
        long id = database.insert(CredentialAdmin.TABLE_NAME, null, values);
        database.close();
        return id;


    }
}
